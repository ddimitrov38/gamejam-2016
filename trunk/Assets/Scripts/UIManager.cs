﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManager : MonoBehaviour {
	public GameObject inventoryBackground;
	public Button inventoryButton;
	public Animation inventoryAnimations;

	public GameObject startScreen;


	// Use this for initialization
	void Start () {
	
	}

	public void hideInventory()
	{
		inventoryBackground.SetActive(false);
		inventoryButton.gameObject.SetActive(true);
		inventoryAnimations.Play("HideInventory");
	}

	public void showInventory()
	{
		inventoryBackground.SetActive(true);
		inventoryButton.gameObject.SetActive(false);
		inventoryAnimations.Play("ShowInventory");
	}


	public void StartGame() 
	{
		inventoryButton.gameObject.SetActive(true);
		startScreen.SetActive(false);
	}

}
