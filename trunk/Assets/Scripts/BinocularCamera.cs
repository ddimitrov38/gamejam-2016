﻿using UnityEngine;
using System.Collections;

public class BinocularCamera : MonoBehaviour {
	public Transform targetCrocodile;
	private bool isCameraSetToCrocodile;
	private float fieldOfView;
	private Camera cameraCrocodile;
	bool isMinimize;
	private GameStateManager gameStateManager;

	public Texture binocularButton;
	public GameObject gameManager;

	// Use this for initialization
	void Start () {
		cameraCrocodile = gameObject.GetComponent("Camera") as Camera;
		isCameraSetToCrocodile = false;//have to be false
		isMinimize = false;//have to be false
		gameStateManager = gameManager.GetComponent<GameStateManager>();
	}
	
	// Update is called once per frame
	void Update () {
		if(isCameraSetToCrocodile == true)
		{
			if(isMinimize)
			{
				cameraCrocodile.fieldOfView = 110f;
			}
			else{
				cameraCrocodile.fieldOfView = 10f;
			}
			transform.LookAt(targetCrocodile);
		}
		else{
			cameraCrocodile.fieldOfView = 60f;
			print("The camera is not set to crocodile");
		}
	}

	public void setIsCameraSetToCrocodile(bool isCameraSet){
		isCameraSetToCrocodile = isCameraSet;
		if (isCameraSet) {
			gameStateManager.setCurrentState (GameStateManager.GameState.BINOCULUS);
		} else {
			gameStateManager.setCurrentState (GameStateManager.GameState.PLAYING);
		}
	}

	void OnGUI(){

		if(isCameraSetToCrocodile){
			if(GUI.Button(new Rect(10,70,50,30), binocularButton)){
				isMinimize = !isMinimize;
			}
		}
	}
}

