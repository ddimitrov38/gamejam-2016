﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventoryObject : UIBehaviour, IPointerEnterHandler, IPointerExitHandler {
	public Texture2D cursorTexture;
	Texture2D oldCursorTexture;

	public bool isCollectable = false;

	public string Name;
	public string InfoText;
	public Sprite itemImage;

	public ObjectsEnum.inventoryObjectsEnum objectType;
	public ObjectsEnum.inventoryObjectsEnum combinesWith;


	Text inventoryLabel;
	bool isCursorOver = false;

	// Use this for initialization
	void Start () {
		inventoryLabel = GameObject.FindGameObjectWithTag("inventoryLabel").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	
		if (isCursorOver) {
			inventoryLabel.rectTransform.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y + 30);
		}
	}



	// Mouse handlers
	void IPointerEnterHandler.OnPointerEnter (PointerEventData eventData)
	{
		if (cursorTexture) {
			Cursor.SetCursor(cursorTexture, new Vector2(cursorTexture.width * 0.5f, cursorTexture.height * 0.5f), CursorMode.Auto);
			inventoryLabel.text = Name;
			isCursorOver = true;
		}
	}

	public void OnPointerExit (PointerEventData eventData)
	{
		if (cursorTexture) {
			Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
			inventoryLabel.text = "";
			isCursorOver = false;
		}
	}


	void OnDestroy() {
		Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
		inventoryLabel.text = "";
	}

}
