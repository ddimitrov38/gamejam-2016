﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class InventoryItem : UIBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
	public InventoryObject invObj;
	GameObject inventoryPanel;
	GameObject UI;
	PlayerController playerController;
	Inventory inventory;
	int touchablesLayer;
	Text combineLabel;

	GameStateManager gameStateManager;


	// Use this for initialization
	void Start () {
		UI = GameObject.FindGameObjectWithTag("UI");
		inventoryPanel = UI.transform.Find("InventoryPanel").gameObject;
		inventory = GameObject.FindGameObjectWithTag("GameController").GetComponent<Inventory>();

		playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

		touchablesLayer = (1 << LayerMask.NameToLayer("TouchableObjects")) | (1 << LayerMask.NameToLayer("inventoryObjects")) | (1 << LayerMask.NameToLayer("Enemy"));

		combineLabel = transform.Find("combineLabel").GetComponent<Text>();

		gameStateManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameStateManager>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetItem(InventoryObject obj)
	{
		invObj = obj;

		GetComponent<Button>().image.overrideSprite = obj.itemImage;
		transform.Find("label").gameObject.GetComponent<Text>().text = obj.Name;
	}

	public void UpdateIcon(Sprite sprite, string labelText)
	{
		GetComponent<Button>().image.overrideSprite = sprite;
		transform.Find("label").gameObject.GetComponent<Text>().text = labelText;
	}


	public void OnBeginDrag (PointerEventData eventData)
	{
		transform.SetParent(UI.transform);
	}

	public void OnDrag (PointerEventData eventData)
	{
		combineLabel.text = "";

		//Camera theCamera = (gameStateManager.currentState == GameStateManager.GameState.PLAYING)?Camera.main:treeCamera;

		RaycastHit hit;
		if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit,  Mathf.Infinity, touchablesLayer)) {
			combineLabel.text = "Combine <i><b>" + invObj.Name + "</b></i> with <i><b>" + hit.collider.GetComponent<InventoryObject>().Name + "</b></i>";
		}
			

		// Check if overlaps with other inventory object
		ObjectsEnum.inventoryObjectsEnum overlappedItem = inventory.RectIsOverlaping(invObj.objectType);
		if (overlappedItem != ObjectsEnum.inventoryObjectsEnum.none) {
			combineLabel.text = "Combine <i><b>" + invObj.Name + "</b></i> with <i><b>" + inventory.GetInventoryObjectForItem(overlappedItem).Name + "</b></i>";
		}

		transform.position = Input.mousePosition;
	}


	public void OnEndDrag (PointerEventData eventData)
	{
		combineLabel.text = "";

		RaycastHit hit;
		if (Physics.Raycast(Camera.main.ScreenPointToRay(transform.position), out hit,  Mathf.Infinity, touchablesLayer)) {
			playerController.CombineItems(this.invObj, hit.collider.GetComponent<InventoryObject>());
		} else {

			// Check UI
			ObjectsEnum.inventoryObjectsEnum overlappedItem = inventory.RectIsOverlaping(invObj.objectType);
			if (overlappedItem != ObjectsEnum.inventoryObjectsEnum.none) {
				playerController.CombineInventory(this.invObj.objectType, overlappedItem);
			}
		}

		transform.SetParent(inventoryPanel.transform);
	}

}
