﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class Inventory : MonoBehaviour {

	// diciionary prefab prefab name kum enum
	Dictionary<ObjectsEnum.inventoryObjectsEnum, GameObject> items;

	public GameObject inventoryItem;
	public GameObject inventoryPanel;


	// Use this for initialization
	void Start () {
		items = new Dictionary<ObjectsEnum.inventoryObjectsEnum, GameObject> ();
	}

	public void addToInventory(InventoryObject invObject)
	{
		GameObject go = GameObject.Instantiate(inventoryItem) as GameObject;
		go.transform.SetParent(inventoryPanel.transform, false);

		go.GetComponent<InventoryItem>().SetItem(invObject);

		items.Add (invObject.objectType, go);
	}

	public void ChangeItem(ObjectsEnum.inventoryObjectsEnum from, ObjectsEnum.inventoryObjectsEnum to)
	{
		items.Add(to, items[from]);
		items.Remove(from);
	}


	public void removeFromInvetory(ObjectsEnum.inventoryObjectsEnum invObject)
	{
		GameObject.Destroy(items[invObject]);
		items.Remove (invObject);
	}
		
	public InventoryItem GetInventoryItemForItem(ObjectsEnum.inventoryObjectsEnum item)
	{
		return items[item].GetComponent<InventoryItem>();
	}

	public InventoryObject GetInventoryObjectForItem(ObjectsEnum.inventoryObjectsEnum item)
	{
		return items[item].GetComponent<InventoryItem>().invObj;
	}

	public ObjectsEnum.inventoryObjectsEnum RectIsOverlaping(ObjectsEnum.inventoryObjectsEnum excluding)
	{
		foreach( KeyValuePair<ObjectsEnum.inventoryObjectsEnum, GameObject> item in items )
		{
			if (excluding == item.Key)
				continue;

			RectTransform itemRect = item.Value.GetComponent<RectTransform>();

			if (RectTransformUtility.RectangleContainsScreenPoint(itemRect, Input.mousePosition))
				return item.Key;
		}

		return ObjectsEnum.inventoryObjectsEnum.none;
	}

}
