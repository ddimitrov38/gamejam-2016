﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameStateManager : MonoBehaviour {

	public enum GameState
	{
		PLAYING,
		CUTSCENE,
		BINOCULUS
	}

	public Dictionary<string, bool> gameScript = new Dictionary<string, bool>() {
		{"haveBook", false},
		{"havePear", false},
		{"haveBinocular", false},
		{"havePincers", false},
		{"haveJar", false},
	};

	public GameState currentState = GameState.PLAYING;

	public void setCurrentState(GameState newState)
	{
		currentState = newState;
	}

	public GameState getCurrentState()
	{
		return currentState;
	}

	public void executeStep(string stepName)
	{
		gameScript [stepName] = true;
	}

	public bool executeAction(string action)
	{
		if (string.Compare (action, "placeBook") == 0) {
			bool value;
			gameScript.TryGetValue("haveBook", out value);
			if (value) {
				// play animation place book
				return true;
			}
			else {
				// do nothing
			}
		}
		if (string.Compare (action, "placeTree") == 0) {
			bool value1, value2, value3, value4;
			gameScript.TryGetValue("havePear", out value1);
			gameScript.TryGetValue("haveBinocular", out value2);
			gameScript.TryGetValue("havePincers", out value3);
			gameScript.TryGetValue("haveJar", out value4);
			if (value1 && value2 && value3 && value4) {
				// play animation place Tree
				return true;
			}
			else {
				// do nothing
			}
		}
		if (string.Compare (action, "useBinocular") == 0) {
			return true;
		}
		if (string.Compare (action, "usePincers") == 0) {
			return true;
		}

		return false;
	}

}
