﻿using UnityEngine;
using System.Collections;

public class TreeCameraController : MonoBehaviour {
	public GameObject treeCameraUI;
	public Camera camera;
	GameStateManager gameStateManager;

	// Use this for initialization
	void Start () {
		treeCameraUI.SetActive(true);
		camera = GetComponent<Camera>();
		gameStateManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameStateManager>();
	}

	// Update is called once per frame
	void Update () {
	
	}

	public void exitCamera()
	{
		Debug.Log("Exiting camera");
		gameStateManager.currentState = GameStateManager.GameState.PLAYING;

		gameObject.SetActive(false);
		treeCameraUI.SetActive(false);
	}

	public void toggleFOV()
	{
		if (camera.fieldOfView == 10)
			camera.fieldOfView = 100;
		else
			camera.fieldOfView = 10;
	}

}
