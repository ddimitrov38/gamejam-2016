A game which me and some friends did for the GameJam contest in Sofia Jan 2016. 
 The game is made from scratch in about 10 hours.
 
 You can find [Online version here](https://dl.dropboxusercontent.com/u/99212230/TROM/index.html)
 
 Here are some screenshots:
 
 ![shot1](http://globalgamejam.org/sites/default/files/styles/game_content__wide/public/games/screenshots/9_6.png?itok=7KGXeJP9)

 ![shot2](http://globalgamejam.org/sites/default/files/styles/game_content__wide/public/games/screenshots/5_56.png?itok=_vAJNHXN)
 
 ![shot3](http://globalgamejam.org/sites/default/files/styles/game_content__wide/public/games/screenshots/7_17.png?itok=phUTRiKU)